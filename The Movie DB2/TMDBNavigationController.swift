//
//  TMDBNavigationController.swift
//  The Movie DB2
//
//  Created by Vojkan Spasic on 8/20/16.
//  Copyright © 2016 Vojkan Spasic. All rights reserved.
//

import UIKit

class TMDBNavigationController: UINavigationController {
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

}
